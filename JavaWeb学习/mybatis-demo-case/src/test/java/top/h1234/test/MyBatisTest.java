package top.h1234.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import top.h1234.mapper.BrandMapper;
import top.h1234.pojo.Brand;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyBatisTest {

    /*查询所有*/
    @Test
    public void testSelectAll() throws IOException {
        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql
        List<Brand> brands = brandMapper.selectAll();
        System.out.println(brands);

        // 5. 释放资源
        sqlSession.close();
    }

    /*根据id查询单条记录*/
    @Test
    public void testSelectOneById() throws IOException {
        // 模拟接收前端传过来的id
        int id = 1;

        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql
        Brand brand = brandMapper.selectOneById(id);
        System.out.println(brand);

        // 5. 释放资源
        sqlSession.close();
    }

    /*多条件查询（多个输入框）*/
    @Test
    public void testSelectByCondition() throws IOException {
        // 模拟接收前端传递的参数
        int status = 0;
        String brand_name = "华为";
        String company_name = "华为";

        // 处理参数
        brand_name = "%" + brand_name + "%";
        company_name = "%" + company_name + "%";

        // 封装对象参数
        Brand brand = new Brand();
        //brand.setStatus(status);
        brand.setBrand_name(brand_name);
        // brand.setCompany_name(company_name);

        // 封装map集合参数
        Map map = new HashMap();
        map.put("status", status);
        map.put("brand_name", brand_name);
        map.put("company_name", company_name);

        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql
        /*散装参数*/
        // List<Brand> brands = brandMapper.selectByCondition(status, brand_name, company_name);

        /*对象参数*/
        // List<Brand> brands = brandMapper.selectByCondition(brand);

        /*map集合参数*/
        // List<Brand> brands = brandMapper.selectByCondition(map);

        /*动态多条件查询（重点）*/
        List<Brand> brands = brandMapper.selectByCondition(brand);
        System.out.println(brands);

        // 5. 释放资源
        sqlSession.close();
    }

    /*单条件查询（下拉选择）*/
    @Test
    public void testSelectByConditionSingle() throws IOException {
        // 模拟接收前端传递的参数
        int status = 0;
        String brand_name = "华为";
        String company_name = "华为";

        // 处理参数
        brand_name = "%" + brand_name + "%";
        company_name = "%" + company_name + "%";

        // 封装对象参数
        Brand brand = new Brand();
        brand.setStatus(status);
        //brand.setBrand_name(brand_name);
        // brand.setCompany_name(company_name);

        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql

        /*动态单条件查询（重点）*/
        List<Brand> brands = brandMapper.selectByConditionSingle(brand);
        System.out.println(brands);

        // 5. 释放资源
        sqlSession.close();
    }

    /*添加*/
    @Test
    public void testAdd() throws IOException {
        // 模拟接收前端传递的参数
        String brand_name = "大米手机2";
        String company_name = "大米2";
        int ordered = 101;
        String description = "大米大米，老鼠爱大米2";
        int status = 1;

        // 封装对象参数
        Brand brand = new Brand();
        brand.setBrand_name(brand_name);
        brand.setCompany_name(company_name);
        brand.setOrdered(ordered);
        brand.setDescription(description);
        brand.setStatus(status);

        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql
        brandMapper.add(brand);
        // 提交事务
        sqlSession.commit();

        // 获取添加成功的id
        Integer id = brand.getId();
        System.out.println(id);

        // 5. 释放资源
        sqlSession.close();
    }

    /*修改*/
    @Test
    public void testUpdate() throws IOException {
        // 模拟接收前端传递的参数
        int id = 7;
        String brand_name = "大米手机2";
        String company_name = "大米2";
        int ordered = 102;
        String description = "大米大米，我要修改你";
        int status = 1;

        // 封装对象参数
        Brand brand = new Brand();
        brand.setId(id);
        brand.setBrand_name(brand_name);
        brand.setCompany_name(company_name);
        brand.setOrdered(ordered);
        brand.setDescription(description);
        brand.setStatus(status);

        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql
        brandMapper.update(brand);
        // 提交事务
        sqlSession.commit();

        // 5. 释放资源
        sqlSession.close();
    }

    /*根据id删除单条记录*/
    @Test
    public void testDeleteById() throws IOException {
        // 模拟接收前端传递的参数
        int id = 7;

        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql
        brandMapper.deleteById(id);
        // 提交事务
        sqlSession.commit();

        // 5. 释放资源
        sqlSession.close();
    }

    /*删除多条记录*/
    @Test
    public void testDeleteByIds() throws IOException {
        // 模拟接收前端传递的参数
        int[] ids = {4, 6};

        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 获取BrandMapper接口的代理对象
        BrandMapper brandMapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 执行sql
        brandMapper.deleteByIds(ids);
        // 提交事务
        sqlSession.commit();

        // 5. 释放资源
        sqlSession.close();
    }
}
