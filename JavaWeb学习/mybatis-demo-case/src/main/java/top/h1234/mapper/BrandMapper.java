package top.h1234.mapper;

import org.apache.ibatis.annotations.Param;
import top.h1234.pojo.Brand;

import java.util.List;
import java.util.Map;

public interface BrandMapper {
    // 查询所有
    List<Brand> selectAll();

    // 根据id，查询单条记录
    Brand selectOneById(@Param("id") int id);

    // 多条件查询（散装参数）
    // List<Brand> selectByCondition(@Param("status") int status, @Param("brand_name") String brand_name, @Param("company_name") String company_name);

    // 多条件查询（对象参数）
    // List<Brand> selectByCondition(Brand brand);

    // 多条件查询（map集合参数）
    // List<Brand> selectByCondition(Map map);

    // 动态多条件查询
    List<Brand> selectByCondition(Brand brand);

    // 动态单条件查询
    List<Brand> selectByConditionSingle(Brand brand);

    // 添加
    void add(Brand brand);

    // 修改
    void update(Brand brand);

    // 根据id删除单条记录
    void deleteById(int id);

    // 删除多条记录
    void deleteByIds(int[] ids);
}
