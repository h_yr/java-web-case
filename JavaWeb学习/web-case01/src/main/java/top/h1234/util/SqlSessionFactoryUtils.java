package top.h1234.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class SqlSessionFactoryUtils {

    // 变量提升，用于在方法中返回出去
    private static SqlSessionFactory sqlSessionFactory;

    // 静态代码块会随着类的加载而自动执行，且只会执行一次
    static {
        try {
            String resource = "mybatis-config.xml";
            InputStream inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 返回工厂
    public static SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

}
