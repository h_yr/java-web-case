package top.h1234.mapper;

import org.apache.ibatis.annotations.Param;
import top.h1234.pojo.User;

public interface UserMapper {
    /*根据用户名、密码查询用户*/
    User select(@Param("username") String username, @Param("password") String password);

    /*根据用户名查询用户*/
    User selectByUsername(String username);

    /*添加用户*/
    void addUser(User user);

}
