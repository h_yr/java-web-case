package top.h1234.web.cookie;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet("/aServlet")
public class AServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 发送cookie

        // 1. 创建cookie对象
        // 存储中文
        String value = "张三";
        value = URLEncoder.encode(value, "utf-8");
        Cookie cookie = new Cookie("username", value);

        // 设置cookie存活时间（这里设置为7天）
        cookie.setMaxAge(24 * 60 * 60 * 7);

        // 2. 发送cookie
        response.addCookie(cookie);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
