package top.h1234.web;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter("/*")
public class FilterDemo implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
//        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // 放行前的逻辑
        System.out.println("1.放行前的逻辑...");

        // 放行
        filterChain.doFilter(servletRequest, servletResponse);

        // 放行后的逻辑（请求完对应的资源之后，会回来执行）
        System.out.println("3.放行后的逻辑...");
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
