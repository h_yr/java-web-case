package org.example;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.mapper.UserMapper;
import org.example.pojo.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.apache.ibatis.io.Resources.*;

public class Main {
    public static void main(String[] args) throws IOException {
        // 1. 加载mybatis核心配置文件，获取 sqlSessionFactory
        String resource = "mybatis-config.xml";
        InputStream inputStream = getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        // 2. 获取SqlSession对象，用于执行sql语句
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 3. 执行sql
        // List<User> users = sqlSession.selectList("test.selectAll");

        // 3.1 获取UserMapper接口的代理对象（主要使用这种）
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = userMapper.selectAll();

        // 4. 打印结果
        System.out.println(users);

        // 5. 释放资源
        sqlSession.close();
    }
}