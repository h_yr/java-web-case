package top.h1234.web.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/*
* Request获取请求数据
* */

@WebServlet("/req1")
public class RequestDemo1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 1. 获取所有参数的Map集合
        Map<String, String[]> map = req.getParameterMap();

        // 获取键
        for (String s : map.keySet()) {
            System.out.print(s + ":");

            // 获取值
            String[] values = map.get(s);
            for (String value : values) {
                System.out.print(value + " ");
            }
            // 换行
            System.out.println();
        }

        System.out.println("------分割线------");

        // 2. 根据key，获取参数值（数组）
        String[] hobbies = req.getParameterValues("hobby");
        // 打印
        for (String hobby : hobbies) {
            System.out.println(hobby);
        }

        System.out.println("------分割线------");

        // 3. 根据key，获取单个参数值
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        System.out.println(username);
        System.out.println(password);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
