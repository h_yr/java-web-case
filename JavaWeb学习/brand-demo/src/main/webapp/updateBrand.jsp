<%--
  Created by IntelliJ IDEA.
  User: 81674
  Date: 2023/1/15
  Time: 21:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>修改</title>
</head>
<body>
<form action="/brand-demo/updateServlet?id=${brand.id}" method="post">
    <p>品牌名称：<input type="text" name="brandName" value="${brand.brandName}"></p>
    <p>企业名称：<input type="text" name="companyName" value="${brand.companyName}"></p>
    <p>排序：<input type="text" name="ordered" value="${brand.ordered}"></p>
    <p>描述信息：<input type="text" name="description" value="${brand.description}"></p>
    <p>
        状态：
        <c:if test="${brand.status == 0}">
            <input type="radio" name="status" value="0" checked> 禁用
            <input type="radio" name="status" value="1"> 启用
        </c:if>
        <c:if test="${brand.status == 1}">
            <input type="radio" name="status" value="0"> 禁用
            <input type="radio" name="status" value="1" checked> 启用
        </c:if>
    </p>
    <p><input type="submit" value="提交"></p>
</form>
</body>
</html>
