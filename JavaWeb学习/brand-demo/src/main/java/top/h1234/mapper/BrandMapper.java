package top.h1234.mapper;

import org.apache.ibatis.annotations.*;
import top.h1234.pojo.Brand;

import java.util.List;

public interface BrandMapper {
    /**
     * 查询所有
     */
    @Select("select * from tb_brand;")
    @ResultMap("brandResultMap")
    List<Brand> selectAll();

    /**
     * 添加
     */
    @Insert("insert into tb_brand values (null,#{brandName},#{companyName},#{ordered},#{description},#{status});")
    void add(Brand brand);

    /**
     * 根据Id查询
     */
    @Select("select * from tb_brand where id = #{id};")
    @ResultMap("brandResultMap")
    Brand selectById(int id);

    /**
     * 根据Id修改
     */
    @ResultMap("brandResultMap")
    void updateById(Brand brand);

    /**
     * 根据Id删除
     */
    @Delete("delete from tb_brand where id = #{id}")
    void deleteById(int id);
}
