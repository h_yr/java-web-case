package top.h1234.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ContextLoaderListener implements ServletContextListener {
    // 加载资源
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Listener监听器随着web应用程序开启而开启...");
    }

    // 销毁资源
    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
