package top.h1234.web;

import top.h1234.pojo.Brand;
import top.h1234.service.BrandService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/selectByIdServlet")
public class SelectByIdServlet extends HttpServlet {
    private BrandService service = new BrandService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 接收id
        String id = request.getParameter("id");

        // 2. 调用service
        Brand brand = service.SelectById(Integer.parseInt(id));

        // 3. 存储到request域中
        request.setAttribute("brand", brand);

        // 4. 转发到updateBrand.jsp
        request.getRequestDispatcher("/updateBrand.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
