package top.h1234.service;

import top.h1234.pojo.Brand;
import top.h1234.pojo.PageBean;

import java.util.List;

public interface BrandService {
    /*查询所有*/
    List<Brand> selectAll();

    /*添加*/
    void add(Brand brand);

    /*删除单条记录*/
    void delete(int id);

    /*删除多条记录*/
    void deleteByIds(int[] ids);

    /**
     * 分页条件查询
     *
     * @param currentPage 当前页码
     * @param pageSize    每页展示记录数
     * @param brand       条件查询对象
     * @return
     */
    PageBean<Brand> selectByPageAndCondition(int currentPage, int pageSize, Brand brand);

    /**
     * 根据ID查询信息
     *
     * @param id
     * @return
     */
    List<Brand> selectById(int id);

    /**
     * 修改
     *
     * @param brand
     */
    void update(Brand brand);
}
