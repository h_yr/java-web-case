package top.h1234.service.impl;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import top.h1234.mapper.BrandMapper;
import top.h1234.pojo.Brand;
import top.h1234.pojo.PageBean;
import top.h1234.service.BrandService;
import top.h1234.util.SqlSessionFactoryUtils;

import java.util.List;

public class BrandServiceImpl implements BrandService {
    // 1. 获取SqlSessionFactoryUtils工厂对象
    SqlSessionFactory factory = SqlSessionFactoryUtils.getSqlSessionFactory();

    /**
     * 查询所有
     *
     * @return
     */
    @Override
    public List<Brand> selectAll() {
        // 2. 获取SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 3. 获取BrandMapper对象
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);
        // 4. 调用方法
        List<Brand> brands = mapper.selectAll();
        // 5. 释放资源
        sqlSession.close();
        // 6. 返回数据
        return brands;
    }

    /**
     * 添加
     *
     * @param brand
     */
    @Override
    public void add(Brand brand) {
        // 2. 获取SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 3. 获取BrandMapper对象
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);
        // 4. 调用方法
        mapper.add(brand);
        // 提交事务
        sqlSession.commit();
        // 5. 释放资源
        sqlSession.close();
    }

    /**
     * 删除单条记录
     *
     * @param id
     */
    @Override
    public void delete(int id) {
        // 2. 获取SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 3. 获取BrandMapper对象
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);
        // 4. 调用方法
        mapper.delete(id);
        // 提交事务
        sqlSession.commit();
        // 5. 释放资源
        sqlSession.close();
    }

    /**
     * 删除多条记录
     *
     * @param ids
     */
    @Override
    public void deleteByIds(int[] ids) {
        // 2. 获取SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 3. 获取BrandMapper对象
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);
        // 4. 调用方法
        mapper.deleteByIds(ids);
        // 提交事务
        sqlSession.commit();
        // 5. 释放资源
        sqlSession.close();
    }

    /**
     * 分页查询
     *
     * @param currentPage 当前页码
     * @param pageSize    每页展示记录数
     * @param brand       条件查询对象
     * @return
     */
    @Override
    public PageBean<Brand> selectByPageAndCondition(int currentPage, int pageSize, Brand brand) {
        // 2. 获取SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 3. 获取BrandMapper对象
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);

        // 4. 处理参数
        // 数据库分页开始的索引
        int beginIndex = (currentPage - 1) * pageSize;

        // brand对象模糊查询
        String brandName = brand.getBrandName();
        if (brandName != null && brandName.length() > 0) {
            brand.setBrandName("%" + brandName + "%");
        }

        String companyName = brand.getCompanyName();
        if (companyName != null && companyName.length() > 0) {
            brand.setCompanyName("%" + companyName + "%");
        }

        // 5. 调用方法
        List<Brand> rows = mapper.selectByPageAndCondition(beginIndex, pageSize, brand);
        int totalCount = mapper.selectTotalCount(brand);

        // 6. 封装到PageBean中
        PageBean<Brand> pageBean = new PageBean<>();
        pageBean.setRows(rows);
        pageBean.setTotalCount(totalCount);

        // 7. 释放资源
        sqlSession.close();

        // 8. 返回数据
        return pageBean;
    }

    /**
     * 根据ID查询信息
     *
     * @param id
     * @return
     */
    @Override
    public List<Brand> selectById(int id) {
        // 2. 获取SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 3. 获取BrandMapper对象
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);
        // 4. 调用方法
        List<Brand> brands = mapper.selectById(id);
        // 5. 释放资源
        sqlSession.close();
        // 6. 返回数据
        return brands;
    }

    /**
     * 修改
     *
     * @param brand
     */
    @Override
    public void update(Brand brand) {
        // 2. 获取SqlSession对象
        SqlSession sqlSession = factory.openSession();
        // 3. 获取BrandMapper对象
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);
        // 4. 调用方法
        mapper.update(brand);
        // 提交事务
        sqlSession.commit();
        // 5. 释放资源
        sqlSession.close();
    }
}
