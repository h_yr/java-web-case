package top.h1234.web.servlet;

import com.alibaba.fastjson.JSON;
import top.h1234.pojo.Brand;
import top.h1234.pojo.PageBean;
import top.h1234.service.BrandService;
import top.h1234.service.impl.BrandServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

@WebServlet("/brand/*")
public class BrandServlet extends BaseServlet {

    // 接口回调
    BrandService brandService = new BrandServiceImpl();

    /*查询所有*/
    public void selectAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 调用方法
        List<Brand> brands = brandService.selectAll();

        // 2. 转为json格式
        String jsonString = JSON.toJSONString(brands);

        // 3. 写数据
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(jsonString);
    }

    /*添加*/
    public void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 获取前端传递的json数据
        BufferedReader reader = request.getReader();
        // json字符串
        String params = reader.readLine();

        // 2. 将json字符串转化为brand对象
        Brand brand = JSON.parseObject(params, Brand.class);

        // 3. 调用方法
        brandService.add(brand);

        // 4. 响应成功的标识
        response.getWriter().write("success");
    }

    /*删除单条记录*/
    public void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 接收前端发送的id
        BufferedReader reader = request.getReader();
        String strId = reader.readLine();

        // 2. 将字符id转换为int型id
        Integer id = JSON.parseObject(strId, int.class);

        // 3. 调用方法
        brandService.delete(id);

        // 4. 响应成功的标识
        response.getWriter().write("success");
    }

    /*删除多条记录*/
    public void deleteByIds(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 接收id数组
        BufferedReader reader = request.getReader();
        String strIds = reader.readLine();

        // 2. 转化为int型
        int[] ids = JSON.parseObject(strIds, int[].class);

        // 3. 调用方法
        brandService.deleteByIds(ids);

        // 4. 响应成功的标识
        response.getWriter().write("success");
    }

    /*分页条件查询*/
    public void selectByPageAndCondition(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 获取前端传递的当前页码和每页展示记录条数
        String _currentPage = request.getParameter("currentPage");
        String _pageSize = request.getParameter("pageSize");

        // 转化为整型
        int currentPage = Integer.parseInt(_currentPage);
        int pageSize = Integer.parseInt(_pageSize);

        // 2. 获取条件查询参数
        BufferedReader reader = request.getReader();
        String params = reader.readLine();

        // 将JSON字符串转为Brand对象
        Brand brand = JSON.parseObject(params, Brand.class);

        // 3. 调用方法
        PageBean<Brand> brandPageBean = brandService.selectByPageAndCondition(currentPage, pageSize, brand);

        // 4. 转化为JSON对象
        String jsonString = JSON.toJSONString(brandPageBean);

        // 5. 返回给前端
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(jsonString);
    }

    /*根据ID查询信息*/
    public void selectById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 接收id
        String _id = request.getParameter("id");

        // 2. 转化为int
        int id = Integer.parseInt(_id);

        // 3. 调用方法
        List<Brand> brands = brandService.selectById(id);

        // 4. 转化为json数据
        String jsonString = JSON.toJSONString(brands);

        // 5. 返回给前端
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(jsonString);
    }

    /*修改*/
    public void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1. 获取前端传递的json数据
        BufferedReader reader = request.getReader();
        // json字符串
        String params = reader.readLine();

        // 2. 将json字符串转化为brand对象
        Brand brand = JSON.parseObject(params, Brand.class);

        // 3. 调用方法
        brandService.update(brand);

        // 4. 响应成功的标识
        response.getWriter().write("success");
    }
}
