package top.h1234.web.servlet.old;

import com.alibaba.fastjson.JSON;
import top.h1234.pojo.Brand;
import top.h1234.service.BrandService;
import top.h1234.service.impl.BrandServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

// @WebServlet("/selectAllServlet")
public class SelectAllServlet extends HttpServlet {

    // 1. 接口回调
    BrandService brandService = new BrandServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 2. 调用方法
        List<Brand> brands = brandService.selectAll();

        // 3. 转为json格式
        String jsonString = JSON.toJSONString(brands);

        // 4. 写数据
        response.setContentType("text/json;charset=utf-8");
        response.getWriter().write(jsonString);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
