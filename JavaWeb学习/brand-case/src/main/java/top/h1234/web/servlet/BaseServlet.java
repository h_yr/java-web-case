package top.h1234.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 自定义Servlet
 * 用于替换HttpServlet（根据请求方式不同进行分发），使用请求的最后一段路径（方法名）进行分发
 */
public class BaseServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 1. 获取请求路径（例如：/brand-case/brand/selectAll）
        String uri = req.getRequestURI();

        // 2. 获取请求路径中最后一段，方法名（selectAll）
        int index = uri.lastIndexOf("/");
        String methodName = uri.substring(index + 1);

        // 3. 获取字节码对象（BrandServlet）
        Class<? extends BaseServlet> cls = this.getClass();

        // 4. 获取方法对象
        try {
            Method method = cls.getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
            // 执行方法
            method.invoke(this, req, resp);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }
}
