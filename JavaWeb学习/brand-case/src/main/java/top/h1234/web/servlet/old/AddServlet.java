package top.h1234.web.servlet.old;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import top.h1234.pojo.Brand;
import top.h1234.service.BrandService;
import top.h1234.service.impl.BrandServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.BufferedReader;
import java.io.IOException;

// @WebServlet("/addServlet")
public class AddServlet extends HttpServlet {

    // 1. 接口回调
    BrandService brandService = new BrandServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 2. 获取前端传递的json数据
        BufferedReader reader = request.getReader();
        // json字符串
        String params = reader.readLine();

        // 3. 将json字符串转化为brand对象
        Brand brand = JSON.parseObject(params, Brand.class);

        // 4. 调用方法
        brandService.add(brand);

        // 5. 响应成功的标识
        response.getWriter().write("success");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
