package top.h1234.pojo;

import java.util.List;

/**
 * 用于封装分页功能返回的数据
 */
public class PageBean<T> {
    // 当前页数据
    private List<T> rows;
    // 总记录数
    private int totalCount;

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
