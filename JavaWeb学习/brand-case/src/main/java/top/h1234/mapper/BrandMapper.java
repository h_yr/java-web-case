package top.h1234.mapper;

import org.apache.ibatis.annotations.*;
import top.h1234.pojo.Brand;

import java.util.List;

public interface BrandMapper {
    /**
     * 查询所有
     *
     * @return
     */
    @Select("select * from tb_brand;")
    @ResultMap("brandResultMap")
    List<Brand> selectAll();

    /**
     * 添加
     *
     * @param brand
     */
    @Insert("insert into tb_brand values (null,#{brandName},#{companyName},#{ordered},#{description},#{status})")
    void add(Brand brand);

    /**
     * 删除单条记录
     *
     * @param id
     */
    @Delete("delete from tb_brand where id = #{id};")
    void delete(int id);

    /**
     * 删除多条记录
     *
     * @param ids
     */
    void deleteByIds(@Param("ids") int[] ids);

    /**
     * 分页条件查询
     *
     * @param beginIndex 开始的索引（开始的索引 = （当前页码 - 1） * 每页展示记录数）
     * @param size       每页展示记录数
     * @param brand      条件查询对象
     * @return
     */
    List<Brand> selectByPageAndCondition(@Param("beginIndex") int beginIndex, @Param("size") int size, @Param("brand") Brand brand);

    /**
     * 根据条件查询总记录数
     *
     * @return
     */
    int selectTotalCount(Brand brand);

    /**
     * 根据ID查询信息
     *
     * @param id
     * @return
     */
    @Select("select * from tb_brand where id = #{id};")
    @ResultMap("brandResultMap")
    List<Brand> selectById(int id);

    /**
     * 修改
     *
     * @param brand
     */
    void update(Brand brand);
}