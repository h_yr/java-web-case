public class Brand {
    // id
    private Integer id;
    // 品牌名称
    private String brand_name;
    // 公司名称
    private String company_name;
    // 排序字段
    private Integer orderd;
    // 描述信息
    private String description;
    // 状态 0：禁用，1：启用
    private Integer status;

    // get set
    public Integer getId() {
        return id;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public String getCompany_name() {
        return company_name;
    }

    public Integer getOrderd() {
        return orderd;
    }

    public String getDescription() {
        return description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public void setOrderd(Integer orderd) {
        this.orderd = orderd;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Brand{" +
                "id=" + id +
                ", brand_name='" + brand_name + '\'' +
                ", company_name='" + company_name + '\'' +
                ", orderd=" + orderd +
                ", description='" + description + '\'' +
                ", status=" + status +
                '}';
    }
}
